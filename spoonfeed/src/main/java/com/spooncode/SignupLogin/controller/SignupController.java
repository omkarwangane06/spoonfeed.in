package com.spooncode.SignupLogin.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.spooncode.SignupLogin.firebaseConfig.DataService;
import com.spooncode.AppPages.controller.MessOwnerPage;
import com.spooncode.AppPages.controller.membership;
import com.spooncode.AppPages.firebaseConfigHome.DataServiceHome;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import java.util.Date;

public class SignupController {
    private LoginController loginController;
    private SimpleDateFormat dateFormat = new SimpleDateFormat();
    private Date dateOfSignup = new Date();
    private Date monthFromSignup;
    private String messUserName;

    public SignupController(LoginController loginController) {
        this.loginController = loginController;
    }

    public Scene createSignupScene(Stage primaryStage, String messUserName) {
        this.messUserName = messUserName;

        // user signup image
        Image user = new Image("./images/user.png");
        ImageView userImgView = new ImageView(user);
        userImgView.setFitHeight(140);
        userImgView.setFitWidth(140);
        userImgView.setPreserveRatio(true);

        // username TextField,PasswordField,addressfield
        Label userLabel = new Label("Username:");
        userLabel.setStyle("-fx-font-weight:bold");
        TextField userTextField = new TextField();
        Label passLabel = new Label("Password:");
        passLabel.setStyle("-fx-font-weight:bold");
        PasswordField passField = new PasswordField();
        Button signupButton = new Button("Add Member");

        Label addressLabel = new Label("Address(for delivery)");
        addressLabel.setStyle("-fx-font-weight:bold");
        TextField addressTextField = new TextField();

        Label phoneNum = new Label("Enter Phone number");
        phoneNum.setStyle("-fx-font-weight:bold");
        TextField phoneNumTextField = new TextField();

        Label role=new Label();

        // Date
        // dateOfSignup=DataServiceHome.currentDate();
        monthFromSignup = DataServiceHome.currentDate();
        // monthFromSignup=DataServiceHome.monthAfter1();

        Label dateLabel = new Label();
        // dateLabel.setText("Joining mess on Date: "+dateFormat.format(dateOfSignup)+"
        // (mm/dd/yyyy)");
        dateLabel.setText("Joining mess on Date: " + dateFormat.format(DataServiceHome.todayDate) + " (mm/dd/yyyy)");

        Label monthLabel = new Label();
        // monthLabel.setText("Membership will expire on:" +
        // dateFormat.format(monthFromSignup) + " (After 30 days)");
        monthLabel.setText("Membership will be expire After 30 days");

        monthLabel.setFont(Font.font(12));
        monthLabel.setStyle("-fx-fond-weight:bold");

        dateLabel.setFont(Font.font(10));
        dateLabel.setStyle("-fx-font-weight:bold");
        monthLabel.setFont(Font.font(10));
        monthLabel.setStyle("-fx-font-weight:bold");
        VBox dateBox = new VBox(8, dateLabel, monthLabel);
        dateBox.setAlignment(Pos.CENTER);

        addressTextField.setPromptText("Enter address");
        addressTextField.setMaxSize(300, 50);

        VBox fieldBox1 = new VBox(10, userLabel, userTextField);
        fieldBox1.setMaxSize(300, 30);
        VBox fieldBox2 = new VBox(10, passLabel, passField);
        fieldBox2.setMaxSize(300, 30);
        VBox fieldBox2_1 = new VBox(10, phoneNum, phoneNumTextField);
        fieldBox2_1.setMaxSize(300, 30);
        VBox fieldBox3 = new VBox(10, addressLabel, addressTextField);
        fieldBox3.setMaxSize(300, 50);

        Button cancel = new Button("Cancel");
        cancel.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                MessOwnerPage messOwnerPage = new MessOwnerPage(primaryStage);
            }

        });

        HBox buttonBox = new HBox(100, cancel, signupButton);
        buttonBox.setMaxSize(350, 30);
        buttonBox.setAlignment(Pos.CENTER);
        signupButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handleSignup(primaryStage, userTextField.getText(), passField.getText(), addressTextField.getText(),
                        Long.parseLong(phoneNumTextField.getText()));
            }
        });

        VBox vbox = new VBox(20, userImgView, fieldBox1, fieldBox2, fieldBox2_1, dateBox, fieldBox3, buttonBox);
        vbox.setAlignment(Pos.CENTER);
        vbox.setFillWidth(true);
        vbox.setStyle("-fx-background-image:url('./images/image.jpg');-fx-background-size:cover");

        /*
         * Scale scale=new Scale(0.75,0.75);
         * vbox.getTransforms().add(scale);
         */
        // StackPane stackPane=new StackPane(vbox);

        // return new Scene(vbox, 600, 600, Color.BLACK);
        // return new Scene(stackPane,600,600,Color.BLACK);
        Scene scene = new Scene(vbox, 1270, 620, Color.BLACK);
        /*
         * vbox.prefWidthProperty().bind(scene.widthProperty());
         * vbox.prefHeightProperty().bind(scene.heightProperty());
         */
        return scene;
    }

    private void handleSignup(Stage primaryStage, String username, String password, String address,Long phoneNum) {
        DataService dataService;
        try {
            dataService = new DataService(primaryStage);
            Map<String, Object> data = new HashMap<>();
            data.put("password", password);
            data.put("username", username);
            data.put("address", address);
            data.put("phoneNum",phoneNum);
            // data.put("JoinedOn",dateFormat.format(dateOfSignup));
            data.put("JoinedOn", dateFormat.format(DataServiceHome.todayDate));
            data.put("expireMembershipOn", dateFormat.format(monthFromSignup));
            data.put("inMess",messUserName);
            data.put("role","member");
            dataService.addData("credentials", "user", "members", username, data);
            System.out.println("User registered successfully");
           // loginController.showLoginScene();
           MessOwnerPage messOwnerPage=new MessOwnerPage(primaryStage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}