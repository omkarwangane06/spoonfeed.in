package com.spooncode.SignupLogin.controller;


import java.util.concurrent.ExecutionException;
import com.spooncode.AppPages.controller.MemberHomePage;
import com.spooncode.AppPages.controller.SecondPage;
import com.spooncode.SignupLogin.dashboards.UserPage;
import com.spooncode.SignupLogin.firebaseConfig.DataService;

import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;


class PassWordMisMatchException extends Exception {
    PassWordMisMatchException(String description) {

        super(description);
    }
}

public class LoginController {

    public static Stage primaryStage;
    private Scene loginScene;
    private Scene userScene;
    private DataService dataService;
    public static String key;
    public static Object db;
    public MemberHomePage memberHomePage;
    String username;
    String messUserName;
   
   
    public LoginController(Stage primaryStage,String messUserName) {
        this.primaryStage = primaryStage;
        this.messUserName=messUserName;
        dataService = new DataService(primaryStage);
        initScenes();

    }

    private void initScenes() {
        initLoginScene();

    }

    public void initLoginScene() {
        Image img = new Image("./images/cooking.png");
        ImageView imgView = new ImageView(img);
        imgView.setFitHeight(150);
        imgView.setFitWidth(150);
        imgView.setPreserveRatio(true);

        Label slogan = new Label("Anything is good if it is made with Love...!");
        slogan.setStyle("-fx-font-weight:bold;-fx-text-fill:black;-fx-font-size:20");
        slogan.setMinSize(250, 30);
        slogan.setAlignment(Pos.CENTER);

        Label messMate = new Label("MessMate");
        messMate.setAlignment(Pos.CENTER);
        messMate.setStyle("-fx-text-fill:Grey");

        Label userLabel = new Label("Username");
        userLabel.setStyle("-fx-text-fill:Black;-fx-font-weight:bold");
        TextField userTextField = new TextField();


        Label passLabel = new Label("Password");
        passLabel.setStyle("-fx-text-fill:Black;-fx-font-weight:bold");
        PasswordField passField = new PasswordField();
        Button loginButton = new Button("Login");

        Button backButton=new Button("Back");

        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    handleLogin(userTextField.getText(), passField.getText());
                   // fadeOutLoginScene();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                passField.clear();
            }
        });
        //loginButton.setStyle("-fx-backgroun-color:#007bff;-fx-text-fill:White;");

        backButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
            try{
               SecondPage secondPage=new SecondPage(primaryStage, messUserName);
              // scaleSceneTransition(loginScene);
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
            }
            
        }); 
       

        VBox fieldBox1 = new VBox(10, userLabel, userTextField);
        fieldBox1.setMaxSize(250, 30);
       
        VBox fieldBox2 = new VBox(10, passLabel, passField);
        fieldBox2.setMaxSize(250, 30);
     
        HBox buttonBox = new HBox(50,backButton, loginButton);
        buttonBox.setMaxSize(350, 30);
        buttonBox.setAlignment(Pos.CENTER);

       
        userTextField.setStyle("-fx-set-pref-width:350");
        passField.setStyle("-fx-set-pref-width:350");

        Image img2 = new Image("./images/image01.png");
        ImageView imgView2 = new ImageView(img2);
        imgView2.setFitHeight(700);
        imgView2.setFitWidth(500);
        imgView2.setPreserveRatio(true);
        imgView2.setOpacity(0.7);


        Label shlokLine1=new Label("अन्नं ब्रह्मा रसं विष्णु भोक्ता देवो जनार्दनम् ।");
        Label shlokLine2=new Label("एवं ध्यात्वा तथा ज्ञात्वा अन्न दोषो न लिप्यते");
        shlokLine1.setStyle("-fx-font-size:20;-fx-font-weight:bold;-fx-text-opacity:0.5");
        shlokLine2.setStyle("-fx-font-size:20;-fx-font-weight:bold;-fx-text-opacity:0.5");
        VBox shlok=new VBox(10,shlokLine1,shlokLine2);
        shlok.setOpacity(0.8);

        Image foodPlateTraditional=new Image("./images/FoodPlateTraditional.png");
        ImageView foodPlate=new ImageView(foodPlateTraditional);
        foodPlate.setFitHeight(500);
        foodPlate.setFitWidth(400);
        foodPlate.setPreserveRatio(true);
        foodPlate.setOpacity(0.1);
        

        VBox vbox = new VBox(20, slogan, fieldBox1, fieldBox2, buttonBox, messMate);
        vbox.setAlignment(Pos.CENTER);
        vbox.setStyle("-fx-background-image:url('./images/image.jpg');-fx-background-size:cover");
       

        //StackPane

        StackPane stackPane=new StackPane(vbox,shlok,foodPlate,imgView2);
       
        stackPane.setAlignment(shlok,Pos.CENTER);
        stackPane.setMargin(shlok, new Insets(70,0,0,470));
        
        stackPane.setAlignment(foodPlate,Pos.CENTER_LEFT);
        stackPane.setMargin(foodPlate, new Insets(80,0,0,-70));
      
        stackPane.setAlignment(imgView2,Pos.CENTER_RIGHT);
        stackPane.setMargin(imgView2, new Insets(80,30,0,0));


        loginScene = new Scene(stackPane,1270,620);
        
        
    }

    private void initUserScene(String username) {

        UserPage userPage = new UserPage(dataService,username);
        userScene = new Scene(userPage.createUserScene(this::handleLogout), 1270, 600);
    }

    public Scene getLoginScene() {
        return loginScene;
    }

    public void showLoginScene() {
        primaryStage.setScene(loginScene);
        primaryStage.setTitle("MessMate");
        primaryStage.show();
    }

    private void handleLogin(String username, String password) throws InterruptedException, ExecutionException {

        dataService.authenticateuserFinal(username, password,messUserName);
        // if(flag==false){
        //     try{
        //    throw new PassWordMisMatchException("Invalid credentials!");
        //     }catch(PassWordMisMatchException e){
        //         String exception=e.toString();
        //         DataService.showAlert("Invalid Info",exception);
        //     }
        // }
    }
    // private void fadeOutLoginScene() {
    //     FadeTransition fadeTransition = new FadeTransition(Duration.seconds(1), loginScene.getRoot());
    //     fadeTransition.setFromValue(1);
    //     fadeTransition.setToValue(0);
    //     fadeTransition.setOnFinished(event -> {
    //         // Navigate to user scene or perform actions after fade out
    //         // Example: initUserScene(username); primaryStage.setScene(userScene);
    //     });
    //     fadeTransition.play();
    // }

    // private void scaleSceneTransition(Scene nextScene) {
    //     ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(0.5), nextScene.getRoot());
    //     scaleTransition.setFromX(0.5);
    //     scaleTransition.setFromY(0.5);
    //     scaleTransition.setToX(1);
    //     scaleTransition.setToY(1);
    //     scaleTransition.setOnFinished(event -> {
    //         primaryStage.setScene(nextScene);
    //         primaryStage.setTitle("MessMate.in");
    //         primaryStage.show();
    //     });
    //     scaleTransition.play();
    // }

    public void showSignupScene(String messUserName) {
        SignupController signupController = new SignupController(this);
        Scene signupScene = signupController.createSignupScene(primaryStage,messUserName);
        primaryStage.setScene(signupScene);
        primaryStage.setTitle("Add Member to Mess");
        primaryStage.show();
    }

    private void handleLogout() {
        primaryStage.setScene(loginScene);
        primaryStage.setTitle("MessMate");
    }
}
