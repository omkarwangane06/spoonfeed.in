package com.spooncode.SignupLogin.dashboards;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
//import com.google.api.services.storage.model.Bucket.Lifecycle.Rule.Action;
import com.google.cloud.firestore.DocumentSnapshot;
//import com.google.firestore.v1.Document;
import com.spooncode.SignupLogin.controller.LoginController;
import com.spooncode.SignupLogin.firebaseConfig.DataService;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class UserPage {
    static String userName;
    private DataService dataService;
    String username;

    VBox vb;

    public UserPage(DataService dataService, String username) {
        this.dataService = dataService;
        this.username = username;
        
    }

    public VBox createUserScene(Runnable logoutHandler) {
        Button logoutButton = new Button("Logout");
        Label dataLabel = new Label();
        System.out.println("createUserScene "+ username);
        try {
            String key = LoginController.key;
            System.out.println("Value of key: " + key);
            // DocumentSnapshot dataObject=dataService.getData("users",key, key, key);
            DocumentSnapshot dataObject = dataService.getData("credentials", "user", "members", username);
            userName = dataObject.getString("username");
            /*
             * DocumentReference docRef = ((DocumentReference)
             * db2).collection("credentials").document("user").collection("members")
             * .document("user");
             * ApiFuture<DocumentSnapshot> snapShot = docRef.get();
             * DocumentSnapshot docSnap = snapShot.get();
             */

            System.out.println("Username fetched: " + userName);
            dataLabel.setText(userName);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        logoutButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                logoutHandler.run();
            }
        });
        Text message = new Text("Welcome " + userName);
        message.setStyle("-fx-text-fill:white;-fx-font-weight:bold;-fx-font-size:60");
        vb = new VBox(350, message, logoutButton);
        vb.setStyle("-fx-background-color:DARKGREY");
        return vb;
    }
}
