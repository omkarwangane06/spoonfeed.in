package com.spooncode.SignupLogin.initialize;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import com.spooncode.AppPages.controller.AdminPage;
import com.spooncode.AppPages.controller.FirstPage;
import com.spooncode.AppPages.controller.MessOwnerPage;
import com.spooncode.AppPages.controller.SecondPage;
import com.spooncode.AppPages.controller.membership;
import com.spooncode.SignupLogin.controller.LoginController;
import com.spooncode.SignupLogin.controller.SignupController;
import com.spooncode.SignupLogin.firebaseConfig.DataService;

import io.grpc.netty.shaded.io.netty.util.NettyRuntime;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class InitializeApp extends Application {

    
    @Override
    public void start(Stage primaryStage) throws IOException, InterruptedException, ExecutionException {

        //LoginController loginController = new LoginController(primaryStage);
        //primaryStage.setScene(loginController.getLoginScene());
        //MemberHomePage memberHomePage=new MemberHomePage(primaryStage);
        // memberHomePage.sceneSecond();
        //SecondPage secondPage = new SecondPage(primaryStage);
        // RequestSignup requestSignup=new RequestSignup(primaryStage);
       // primaryStage.setHeight(600);
       // primaryStage.setWidth(1000);
       // primaryStage.setMinHeight(500);
        //primaryStage.setMinWidth(800);
        ////////////////////////////////DataService dataService=new DataService(primaryStage);

       // dataService.initializeFirebase();
        primaryStage.setTitle("MessMate.in");
        primaryStage.getIcons().add(new Image("./images/cooking.png"));
        //membership mem=new membership(primaryStage);
        //primaryStage.setMaximized(true);
        //primaryStage.initStyle(StageStyle.DECORATED);
        
        FirstPage firstPage=new FirstPage(primaryStage);
//AdminPage adminPage=new AdminPage(primaryStage);
//adminPage.initAdminPage();
//MessOwnerPage messOwnerPage=new MessOwnerPage(primaryStage);

       // primaryStage.show();
    }
}
