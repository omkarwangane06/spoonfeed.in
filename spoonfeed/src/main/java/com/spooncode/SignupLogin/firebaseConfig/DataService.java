package com.spooncode.SignupLogin.firebaseConfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.HashMap;
import java.util.Map;
import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.spooncode.AppPages.controller.AdminPage;
import com.spooncode.AppPages.controller.MemberHomePage;
import com.spooncode.AppPages.controller.MessOwnerPage;
import com.spooncode.AppPages.controller.SecondPage;

import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.lang.String;

class PassWordMisMatchException extends Exception {
    PassWordMisMatchException(String description) {

        super(description);
    }
}

class WrongInfoException extends Exception {
    WrongInfoException(String description) {
        super(description);
    }
}
class IlleagalLoginException extends Exception{
    IlleagalLoginException(String message){
        super(message);
    }
}
public class DataService {
    private static Stage primaryStage;

    public DataService(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public static Firestore db;
    static {
        try {
            initializeFirebase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void initializeFirebase() throws IOException {
        FileInputStream serviceAccount = new FileInputStream(
                "D:\\MessMateFinal\\spoonfeed.in\\spoonfeed\\src\\main\\resources\\fireStoreMessMate.json");
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .build();
        FirebaseApp.initializeApp(options);
        db = FirestoreClient.getFirestore();

    }

    public static void addMenuInitial(String messName) {
        Map<String, Object> addMenuInitialMap = new HashMap<>();

        addMenuInitialMap.put("item1", null);
        addMenuInitialMap.put("item1Url", null);
        addMenuInitialMap.put("item2Url", null);
        addMenuInitialMap.put("item3Url", null);
        addMenuInitialMap.put("item4Url", null);
        addMenuInitialMap.put("item5Url", null);
        ApiFuture<WriteResult> documentReference = db.collection("menu").document(messName).set(addMenuInitialMap);

    }

    public void addData(String collection, String document, String collection2, String document2,
            Map<String, Object> data)
            throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection(collection).document(document).collection(collection2)
                .document(document2);
        ApiFuture<WriteResult> result = docRef.set(data);
        result.get();
    }

    public DocumentSnapshot getData(String collection, String document, String collection2, String document2)
            throws ExecutionException, InterruptedException {

        try {
            DocumentReference docRef = db.collection(collection).document(document).collection(collection2)
                    .document(document2);
            ApiFuture<DocumentSnapshot> future = docRef.get();
            return future.get();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public boolean authenticateUser1(String username, String password) throws ExecutionException, InterruptedException {

        DocumentSnapshot document = db.collection("credentials").document("user").collection("members")
                .document(username).get().get();

        if (document.exists()) {
            String storedPassword = document.getString("password");
            return password.equals(storedPassword);
        }
        return false;
    }

    public boolean authenticateUser2(String username, String password) throws InterruptedException, ExecutionException {
        DocumentReference docRef = db.collection("credentials").document("user").collection("members")
                .document(username);
        ApiFuture<DocumentSnapshot> snapShot = docRef.get();
        DocumentSnapshot docSnap = snapShot.get();
        System.out.println(docSnap.get("username"));

        if (docSnap.exists()) {
            System.out.println(docSnap.get("address"));
            // String userNameString = docSnap.getString(username);
            String storedPassword = docSnap.getString("password");
            System.out.println(storedPassword);
            return password.equals(storedPassword);
        } else {
            return false;
        }
    }

    // public boolean authenticateUser3(String username, String password) throws InterruptedException, ExecutionException {

    //     DocumentReference docRef = db.collection("credentials").document("user").collection("members")
    //             .document(username);

    //     ApiFuture<DocumentSnapshot> snapShot = docRef.get();
    //     DocumentSnapshot docSnap = snapShot.get();

    //     if (docSnap.exists()) {
    //         String storedPassword = docSnap.getString("password");
    //         System.out.println(storedPassword);
    //         return password.equals(storedPassword);

    //     }

    //     else if ((String) docSnap.get("role") != "member") {

    //         System.out.println("inside elseIf");
    //         authenticateAdmin(username, password);
    //         return false;
    //     } else {
    //         authenticateAdmin(username, password);
    //         return false;
    //     }
    // }

    /*
     * if (docSnap.exists()) {
     * if (docSnap.get("role") == "member") {
     * String storedPassword = docSnap.getString("password");
     * System.out.println(storedPassword);
     * return password.equals(storedPassword);
     * }
     * 
     * else if ((String) docSnap.get("role") != "member") {
     * authenticateAdmin(username, password);
     * return false;
     * }
     * 
     * return false;
     * 
     * } else {
     * return false;
     * }
     * 
     */

    /*
     * else{
     * System.out.println(docSnap.get("username"));
     * 
     * if (docSnap.exists()) {
     * System.out.println(docSnap.get("address"));
     * // String userNameString = docSnap.getString(username);
     * String storedPassword = docSnap.getString("password");
     * System.out.println(storedPassword);
     * return password.equals(storedPassword);
     * } else {
     * return false;
     * }
     */

    byte flag = 0;

    public void authenticateuserFinal(String username, String password, String messUserName)
            throws InterruptedException, ExecutionException {

        DocumentReference documentReference = db.collection("credentials").document("user").collection("members")
                .document(username);
        ApiFuture<DocumentSnapshot> snapshot = documentReference.get();
        DocumentSnapshot docSnap = snapshot.get();

        if (docSnap.exists()) {

            if (docSnap.getString("inMess").equals(messUserName)) {

                String storedMemberPass = docSnap.getString("password");

                if (password.equals(storedMemberPass)) {
                    try {
                        SecondPage secondPage = new SecondPage(primaryStage, username);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                } else {
                    try {
                        throw new PassWordMisMatchException("You entered invalid Username or password!");
                    } catch (PassWordMisMatchException e) {
                        showAlert("Invalid credentials!!!!", e.getMessage());

                    }
                }
            }
            else{
                try{
                throw new IlleagalLoginException("You are not member of this mess!");
                }catch(IlleagalLoginException e){
                    showAlert("Illeagal Login",e.getMessage());
                }
            }

        } else {
            authenticateAdmin(username, password,messUserName);
        }

    }

    public void authenticateAdmin(String username, String password,String messUserName) throws InterruptedException, ExecutionException {
        DocumentReference docRef = db.collection("credentials").document("user").collection("admin")
                .document(username);
        ApiFuture<DocumentSnapshot> snapShot = docRef.get();
        DocumentSnapshot docSnap = snapShot.get();

        if (docSnap.exists()) {
            if (docSnap.get("role").equals("admin")) {

                String storedPassword = docSnap.getString("adminPassword");
                // return password.equals(storedPassword);
                if (password.equals(storedPassword)) {
                    AdminPage adminPage = new AdminPage(primaryStage);
                } else {
                    try {
                        throw new PassWordMisMatchException("You entered wrong Admin password!!");
                    } catch (PassWordMisMatchException e) {
                        showAlert("Wrong credentials!", e.getMessage());
                    }
                }
            } else {
                authenticateMessOwner(username, password,messUserName);
            }
        } else {
            authenticateMessOwner(username, password,messUserName);
        }
    }

    public void authenticateMessOwner(String username, String password,String messUserName) {
      

        try {
            System.out.println("messUserName   "+messUserName);
            System.out.println("username   "+username);
            if(messUserName.equals(username)){
 
            DocumentReference docRef = db.collection("credentials").document("user").collection("messOwner")
                    .document(username);

            
            ApiFuture<DocumentSnapshot> snapMessOwner = docRef.get();
            DocumentSnapshot messOwnerSnap = snapMessOwner.get();
            if (messOwnerSnap.exists()) {

                boolean flag = false;
                String temp = messOwnerSnap.getString("role");
                if ((temp == "messOwner")) {
                    flag = true;
                } else {
                    flag = false;
                }

                if (messOwnerSnap.getString("role").equals("messOwner")) {
                    String storedPass = (String) messOwnerSnap.get("password");
                    System.out.println("inside rolecheck," + storedPass);
                    if (password.equals(storedPass)) {
                        MessOwnerPage messOwnerPage = new MessOwnerPage(primaryStage);
                    }
                    else {
                        try {
                            throw new PassWordMisMatchException("You entered wrong MessOwner Password");
                        } catch (PassWordMisMatchException e) {
    
                            showAlert("Invalid password!", e.getMessage());
                        }
                    }
                   

                } else {
                    try {
                        throw new PassWordMisMatchException("You entered wrong MessOwner Password");
                    } catch (PassWordMisMatchException e) {

                        showAlert("Invalid password!", e.getMessage());
                    }
                }
            } else {
                try {
                    throw new WrongInfoException("Please Enter correct information");
                } catch (Exception e) {
                    showAlert("Invalid Credentials!", e.getMessage());
                }
            }
        }else{
            try{
           throw new IlleagalLoginException ("You are trying to login from another mess! Please use your own dashboard");
            }catch(Exception e){
                showAlert("Illeagal Login", e.getMessage());
            }
        }
        } catch (Exception e) {
            e.printStackTrace();

        }
    

    }

    public static void showAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setResizable(false);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

}
