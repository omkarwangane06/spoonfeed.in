package com.spooncode.AppPages.firebaseConfigHome;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.print.Doc;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.spooncode.AppPages.controller.MessOwnerPage;
import com.spooncode.SignupLogin.firebaseConfig.DataService;

public class DataServiceHome {
    private static Firestore db;
    public static Date todayDate = new Date();

    static DocumentSnapshot messDocSnap;

    /*
     * public static void initializeFirebase() throws IOException {
     * FileInputStream serviceAccount = new FileInputStream(
     * "D:\\\\MessMateFinal\\\\spoonfeed.in\\\\spoonfeed\\\\src\\\\main\\\\resources\\\\fireStoreMessMate.json"
     * );
     * 
     * FirebaseOptions options = new FirebaseOptions.Builder()
     * .setCredentials(GoogleCredentials.fromStream(serviceAccount))
     * .build();
     * 
     * FirebaseApp.initializeApp(options);
     * db = FirestoreClient.getFirestore();
     * 
     * 
     * }
     */

    // I commented above initialization of firebase becauseb it is already done in
    // Dataservice file of SignupLogin.firebaseConfig package hence i was facing
    // exceotion

    public static void initializeFirebase() {
        db = DataService.db;
    }

    // Here i'm going to read collection messOwner to fetch mess information and
    // details

    public static DocumentSnapshot messData(String messUserName)
            throws InterruptedException, ExecutionException, IOException {
        initializeFirebase();

        String mess = messUserName;

        DocumentReference docRef = db.collection("credentials").document("user").collection("messOwner")
                .document(mess);
        ApiFuture<DocumentSnapshot> snapShot = docRef.get();
        DocumentSnapshot docSnap = snapShot.get();
        return docSnap;
    }

    public static DocumentSnapshot messMenu(String messUserName) throws InterruptedException, ExecutionException {
        DocumentReference docRef = db.collection("menu").document(messUserName);
        ApiFuture<DocumentSnapshot> messDoc = docRef.get();
        DocumentSnapshot messDocSnap = messDoc.get();
        return messDocSnap;

    }

    public static Date currentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("mm/dd/yyyy");
        // Date todayDate=new Date();
        dateFormat.format(todayDate);

        Calendar cal = Calendar.getInstance();
        cal.setTime(todayDate);
        cal.add(Calendar.DATE, 30);
        Date monthAfter = cal.getTime();
        return monthAfter;
        // return todayDate;
    }

    public static LocalDate monthAfter() {
        LocalDate localDate = LocalDate.now();
        LocalDate monthFromSignup = localDate.plusDays(30);
        return monthFromSignup;

    }

    public static Date monthAfter1() {
        Date todayDate2 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(todayDate2);
        cal.add(Calendar.DATE, 30);
        Date monthAfter = cal.getTime();
        return monthAfter;
    }

    public static void updateMenuData(String messUserName) throws InterruptedException, ExecutionException {

        DocumentReference docRef = db.collection("menu").document(messUserName);

        Map<String, Object> updateMenu = new HashMap<>();
        updateMenu.put("item1", MessOwnerPage.item1.getText());
        updateMenu.put("item2", MessOwnerPage.item2.getText());
        updateMenu.put("item3", MessOwnerPage.item3.getText());
        updateMenu.put("item3", MessOwnerPage.item3.getText());
        updateMenu.put("item4", MessOwnerPage.item4.getText());
        updateMenu.put("item5", MessOwnerPage.item5.getText());
        updateMenu.put("price", MessOwnerPage.priceField.getText());
        docRef.set(updateMenu);
    }

}
