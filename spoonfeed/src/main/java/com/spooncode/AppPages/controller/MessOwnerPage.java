package com.spooncode.AppPages.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.spooncode.AppPages.firebaseConfigHome.DataServiceHome;
import com.spooncode.SignupLogin.controller.LoginController;
import com.spooncode.SignupLogin.firebaseConfig.DataService;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MessOwnerPage {
    private Stage primaryStage;
    private Firestore db;
    public String messUserName = SecondPage.messUserName;

    public static TextField item1;
    public static TextField item2;
    public static TextField item3;
    public static TextField item4;
    public static TextField item5;
    public static TextField priceField;
    public DocumentSnapshot docSnap;

    public MessOwnerPage(Stage primaryStage) {
        this.primaryStage = primaryStage;
        initMessOwnerPage();
    }

    void initMessOwnerPage() {

        primaryStage.setTitle("MessMate MessOwner Dashboard");
        Label hey = new Label("Hello Owner!!");
        hey.setStyle("-fx-font-size:20px;-fx-font-weight:bold");
        Label line = new Label("Manage your mess with your Mate!");
        line.setStyle("-fx-font-size:25px;-fx-font-weight:bold;-fx-text-fill:#FF8C00");

        VBox vb0 = new VBox(6, hey, line);
        vb0.setAlignment(Pos.CENTER);

        Button addMember = new Button();
        Image addMem = new Image("./images/addMember.png");
        ImageView imgViewAddMem = new ImageView(addMem);
        imgViewAddMem.setFitWidth(130);
        imgViewAddMem.setFitHeight(130);
        imgViewAddMem.setPreserveRatio(true);
        addMember.setGraphic(imgViewAddMem);
        Label addMemLabel = new Label("Add new Member");
        addMemLabel.setStyle("-fx-font-weight:bold");
        addMemLabel.setFont(Font.font(15));
        VBox vb1 = new VBox(5, addMember, addMemLabel);
        vb1.setAlignment(Pos.CENTER);
        addMember.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                LoginController loginController = new LoginController(primaryStage, messUserName);
                loginController.showSignupScene(messUserName);
            }

        });

        Button addMenu = new Button();
        Image addTodayMenu = new Image("./images/menu.png");
        ImageView imgViewMenu = new ImageView(addTodayMenu);
        imgViewMenu.setFitWidth(130);
        imgViewMenu.setFitHeight(130);
        imgViewMenu.setPreserveRatio(true);
        addMenu.setGraphic(imgViewMenu);
        Label addMenuLabel = new Label("Update todays menu");
        addMenuLabel.setStyle("-fx-font-weight:bold");
        addMenuLabel.setFont(Font.font(15));
        VBox vb2 = new VBox(5, addMenu, addMenuLabel);
        vb2.setAlignment(Pos.CENTER);
        addMenu.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                try {
                    updateMenu();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });

        Button addAttendance = new Button();
        Image attendanceImage = new Image("./images/attendance.png");
        ImageView imgViewAttendence = new ImageView(attendanceImage);
        imgViewAttendence.setFitWidth(130);
        imgViewAttendence.setFitHeight(130);
        imgViewAttendence.setPreserveRatio(true);
        addAttendance.setGraphic(imgViewAttendence);
        Label addAttendenceLabel = new Label("Today's Attendance");
        addAttendenceLabel.setStyle("-fx-font-weight:bold");
        addAttendenceLabel.setFont(Font.font(15));
        VBox vb3 = new VBox(5, addAttendance, addAttendenceLabel);
        vb3.setAlignment(Pos.CENTER);

        Button myMembers = new Button();
        Image myMembersImage = new Image("./images/customer.png");
        ImageView imgViewMyMembers = new ImageView(myMembersImage);
        imgViewMyMembers.setFitWidth(130);
        imgViewMyMembers.setFitHeight(130);
        imgViewMyMembers.setPreserveRatio(true);
        myMembers.setGraphic(imgViewMyMembers);
        Label myMembersLabel = new Label("View my members");
        myMembersLabel.setStyle("-fx-font-weight:bold");
        myMembersLabel.setFont(Font.font(15));
        VBox vb4 = new VBox(5, myMembers, myMembersLabel);
        vb4.setAlignment(Pos.CENTER);

        Button myMess = new Button();
        Image myMessImg = new Image("./images/account.png");
        ImageView imgViewMyMess = new ImageView(myMessImg);
        imgViewMyMess.setFitWidth(130);
        imgViewMyMess.setFitHeight(130);
        imgViewMyMess.setPreserveRatio(true);
        myMess.setGraphic(imgViewMyMess);
        Label myMessLabel = new Label("My Profile");
        myMessLabel.setStyle("-fx-font-weight:bold");
        myMessLabel.setFont(Font.font(15));
        VBox vb5 = new VBox(5, myMess, myMessLabel);
        vb5.setAlignment(Pos.CENTER);
        myMess.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                try {
                    RequestSignup requestSignup = new RequestSignup(primaryStage, messUserName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Label messMateLabel = new Label("MessMate");
        messMateLabel.setStyle("-fx-text-fill:Grey");

        HBox hb1 = new HBox(100, vb1, vb2);
        hb1.setAlignment(Pos.CENTER);
        HBox hb2 = new HBox(100, vb3, vb4, vb5);
        hb2.setAlignment(Pos.CENTER);

        Button logout = new Button("MessOwner Logout");
        logout.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                LoginController loginController = new LoginController(primaryStage, messUserName);
                loginController.showLoginScene();
            }

        });
        

        VBox vbOuter = new VBox(40, vb0, hb1, hb2, logout, messMateLabel);
        vbOuter.setAlignment(Pos.CENTER);
        vbOuter.setPadding(new Insets(30, 0, 0, 0));
        vbOuter.setStyle("-fx-background-image:url('./images/image.jpg');-fx-background-size:cover");

        Scene messOwnerScene = new Scene(vbOuter, 1270, 620);
        primaryStage.setScene(messOwnerScene);
        primaryStage.show();

    }


    public void updateMenu() {

        db = DataService.db;
        Image topImg = new Image("./images/menuTopImg.png");
        ImageView topImgView = new ImageView(topImg);
        topImgView.setFitHeight(60);
        topImgView.setFitWidth(60);
        Label heyMessOwner = new Label("Hey Mess Owner!!");
        heyMessOwner.setStyle("-fx-font-size:20px");
        Label updateYourMenu = new Label("Update Your today's menu...");
        updateYourMenu.setStyle("-fx-font-weight:bold;-fx-font-size:25px;-fx-text-fill:#FF8C00");
        VBox LabelBox = new VBox(5, heyMessOwner, updateYourMenu);
        LabelBox.setAlignment(Pos.CENTER);

        HBox headerBox = new HBox(10, topImgView, LabelBox);
        headerBox.setAlignment(Pos.CENTER);

        Label item1Label = new Label("Enter item 1");
        item1Label.setStyle("-fx-font-weight:bold");
        item1 = new TextField();
        VBox itemBox1 = new VBox(item1Label, item1);
        itemBox1.setAlignment(Pos.CENTER);
        item1.setMaxWidth(600);

        Label item2Label = new Label("Enter item 2");
        item2Label.setStyle("-fx-font-weight:bold");
        item2 = new TextField();
        item2.setMaxWidth(600);
        VBox itemBox2 = new VBox(item2Label, item2);
        itemBox2.setAlignment(Pos.CENTER);

        Label item3Label = new Label("Enter item 3");
        item3Label.setStyle("-fx-font-weight:bold");
        item3 = new TextField();
        item3.setMaxWidth(600);
        VBox itemBox3 = new VBox(item3Label, item3);
        itemBox3.setAlignment(Pos.CENTER);

        Label item4Label = new Label("Enter item 4");
        item4Label.setStyle("-fx-font-weight:bold");
        item4 = new TextField();
        item4.setMaxWidth(600);
        VBox itemBox4 = new VBox(item4Label, item4);
        itemBox4.setAlignment(Pos.CENTER);

        Label item5Label = new Label("Enter item 5");
        item5Label.setStyle("-fx-font-weight:bold");
        item5 = new TextField();
        item5.setMaxWidth(600);
        VBox itemBox5 = new VBox(item5Label, item5);
        itemBox5.setAlignment(Pos.CENTER);

        Label priceLabel = new Label("Enter Plate Price");
        item5Label.setStyle("-fx-font-weight:bold");
        priceField = new TextField();
        priceField.setMaxWidth(600);
        VBox priceBox = new VBox(priceLabel, priceField);
        priceBox.setAlignment(Pos.CENTER);

        Button cancel = new Button("Cancel");
        cancel.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                initMessOwnerPage();
            }

        });

        Button submit = new Button("Update menu");
        submit.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                try {
                    DataServiceHome.updateMenuData(messUserName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println("Menu updated successfully!!");
                MessOwnerPage messOwnerPage = new MessOwnerPage(primaryStage);

            }

        });

        // Button back=new Button("Cancel");
        // back.setOnAction(new EventHandler<ActionEvent>() {

        // @Override
        // public void handle(ActionEvent event) {
        // MessOwnerPage messOwnerPage=new MessOwnerPage(primaryStage);
        // }

        // });
        HBox buttonBox = new HBox(100, cancel, submit);
        buttonBox.setAlignment(Pos.CENTER);

        VBox FinalBox = new VBox(10, headerBox, itemBox1, itemBox2, itemBox3, itemBox4, itemBox5, priceBox, buttonBox);
        FinalBox.setStyle("-fx-background-image:url('./images/abc.jpg');-fx-background-size:cover");
        FinalBox.setAlignment(Pos.CENTER);
        StackPane stackPane = new StackPane(FinalBox);
        Scene scene = new Scene(stackPane, 1270, 620);
        primaryStage.setScene(scene);

    }

    void myProfile(String messUserName){
        try{
        docSnap=DataServiceHome.messData(messUserName);
        }catch(Exception e){
            System.out.println(e);
        }

        Image logo=new Image(docSnap.getString("logo"));
        ImageView logoView=new ImageView(logo);
        logoView.setFitHeight(200);
        logoView.setFitWidth(200);
        logoView.setPreserveRatio(true);

        Label messName=new Label(docSnap.getString("messName"));
        messName.setStyle("-fx-font-size:20;-fx-font-weight:bold;-fx-text-fill:#FF8C00");

        Label ownerName=new Label("Mess Owner: "+docSnap.getString("ownerName"));
        ownerName.setStyle("-fx-font-size:15;-fx-font-weight:bold;-fx-text-fill:Black");

        Label phone=new Label("Contact number: "+docSnap.getString("messTime"));
        phone.setStyle("-fx-font-size:15;-fx-font-weight:bold;-fx-text-fill:Black");

        Label time=new Label("Time: "+docSnap.getString("ownerName"));
        time.setStyle("-fx-font-size:15;-fx-font-weight:bold;-fx-text-fill:Black");

        Label isNonVeg=new Label("Veg or Non-Veg: "+docSnap.getString("isNonVeg"));
        isNonVeg.setStyle("-fx-font-size:15;-fx-font-weight:bold;-fx-text-fill:Black");

        Label messFees=new Label("Mess Fees: "+docSnap.getString("messFees"));
        messFees.setStyle("-fx-font-size:15;-fx-font-weight:bold;-fx-text-fill:Black");

        Label extra=new Label("Mess Owner: "+docSnap.getString("extraRules"));
        extra.setStyle("-fx-font-size:15;-fx-font-weight:bold;-fx-text-fill:Black");

        Label address=new Label("Mess Address: "+docSnap.getString("messAddress"));
        address.setStyle("-fx-font-size:15;-fx-font-weight:bold;-fx-text-fill:Black");

        Label holiday=new Label("When Holiday: "+docSnap.getString("whenHoliday"));
        holiday.setStyle("-fx-font-size:15;-fx-font-weight:bold;-fx-text-fill:Black");

    }

}
