package com.spooncode.AppPages.controller;

import com.spooncode.SignupLogin.controller.LoginController;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class AdminPage {
    private static Stage primaryStage;
    private static String messUserName=SecondPage.messUserName;

    public AdminPage(Stage primaryStage) {
        this.primaryStage = primaryStage;
        initAdminPage();
    }
    public static void initAdminPage(){
      
        Label hey=new Label("Hey admin!");
        hey.setStyle("-fx-font-weight:bold;-fx-text-fill:Black");
        hey.setFont(Font.font(15));
        Label talk=new Label("Handle your Application!");
        talk.setStyle("-fx-font-weight:bold;-fx-text-fill:#FF8C00");
        talk.setFont(Font.font(20));
        Button addMess=new Button("Add new Mess");
        addMess.setFont(Font.font(20));
        addMess.setStyle("-fx-text-fill:White;-fx-background-color:Black");
        addMess.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                AddNewMess addNewMess=new AddNewMess(primaryStage);
            }
            
        });

        Button logout=new Button("Admin logout");
        logout.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               LoginController loginController=new LoginController(primaryStage,messUserName);
               primaryStage.setScene(loginController.getLoginScene());
            }
            
        });

        VBox vb=new VBox(10,hey,talk,addMess,logout);
        vb.setAlignment(Pos.CENTER);
        vb.setStyle("-fx-background-image:url('./images/image.jpg');-fx-background-size:cover");
        
        if(AddNewMess.flag>0){
            vb.setLayoutX(-400);
            vb.setLayoutY(-300);

        }
        Scene sc=new Scene(vb,1270,620);

        primaryStage.setScene(sc);
        primaryStage.show();
    }
    


}
