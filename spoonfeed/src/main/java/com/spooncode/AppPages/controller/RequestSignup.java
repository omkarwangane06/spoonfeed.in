package com.spooncode.AppPages.controller;

import java.io.IOException;
import java.rmi.dgc.Lease;
import java.util.concurrent.ExecutionException;

import com.google.cloud.firestore.DocumentSnapshot;
import com.google.type.Color;
import com.spooncode.AppPages.firebaseConfigHome.DataServiceHome;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class RequestSignup {
    public Stage primaryStage;
    String messUserName;

    public RequestSignup(Stage primaryStage,String messUserName) throws InterruptedException, ExecutionException, IOException {
        this.primaryStage = primaryStage;
        this.messUserName=messUserName;
        initRequestSignup();

    }

    void initRequestSignup() throws InterruptedException, ExecutionException, IOException {

        Label welcome = new Label("Welcome to our mess!");
        welcome.setFont(Font.font(20));
        welcome.setStyle("-fx-text-fill:#FF8C00;-fx-font-weight:bold");

        Label notice = new Label("If you want to be our member,please read our information clearly.");
        notice.setFont(Font.font(18));
        notice.setStyle("-fx-font-weight:bold;-fx-text-fill:#3CB371");

        DocumentSnapshot messDataSnap = DataServiceHome.messData(messUserName);

        Label messName = new Label();
        messName.setText("Mess Name: " + (String) messDataSnap.get("messName"));
        messName.setFont(Font.font(15));
        messName.setStyle("-fx-font-weight:bold;-fx-text-fill:#000000");

        Label messTime = new Label("Mess time: " + (String) messDataSnap.get("messTime"));
        messTime.setFont(Font.font(15));
        messTime.setStyle("-fx-font-weight:bold;-fx-text-fill:#000000");

        Label messFees = new Label("Month fees: " + (String) messDataSnap.get("messFees"));
        messFees.setFont(Font.font(15));
        messFees.setStyle("-fx-font-weight:bold;-fx-text-fill:#000000");

        Label whenHoliday = new Label("When holidays: " + (String) messDataSnap.get("whenHoliday"));
        whenHoliday.setFont(Font.font(15));
        whenHoliday.setStyle("-fx-font-weight:bold;-fx-text-fill:#000000");

        Label messAddress = new Label("Mess Address:" + (String) messDataSnap.get("messAddress"));
        messAddress.setFont(Font.font(15));
        messAddress.setStyle("-fx-font-weight:bold;-fx-text-fill:#000000");

        Label isNonVeg = new Label("Is nonVeg available: " + (String) messDataSnap.get("isNonVeg"));
        isNonVeg.setFont(Font.font(15));
        isNonVeg.setStyle("-fx-font-weight:bold;-fx-text-fill:#000000");

        Label extraRules = new Label("Additionally consider : " + (String) messDataSnap.get("extraRules"));
        extraRules.setFont(Font.font(15));
        extraRules.setStyle("-fx-font-weight:bold;-fx-text-fill:#000000");

        Label contactLabel = new Label("If you are intrested,contact: " + (String) messDataSnap.get("contact"));
        contactLabel.setFont(Font.font(17));
        contactLabel.setStyle("-fx-font-weight:bold;-fx-text-fill:#3CB371");

        Label ownerName = new Label("Mess Owner: " + (String) messDataSnap.get("ownerName"));
        ownerName.setFont(Font.font(17));
        ownerName.setStyle("-fx-font-weight:bold;-fx-text-fill:#3CB371");

        Button back=new Button("Back to Home Page");
        back.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
             try {
                SecondPage secondPage=new SecondPage(primaryStage, messUserName);
            } catch (Exception e) {
                e.printStackTrace();
            }
            }
            
        });

       //Button membership=new Button("Take membership");


        VBox vb1 = new VBox(10, welcome, notice);
        vb1.setAlignment(Pos.CENTER);
        VBox vb2 = new VBox(10, messName, messTime, messFees, isNonVeg, whenHoliday, messAddress, extraRules);
        vb2.setAlignment(Pos.CENTER);
        VBox vb3 = new VBox(10, ownerName, contactLabel);
        vb3.setAlignment(Pos.CENTER);

        VBox vbOuter = new VBox(15, vb1, vb2, vb3,back);
        vbOuter.setAlignment(Pos.CENTER);
        vbOuter.setStyle("-fx-background-image:url('./images/abc2.jpg');-fx-background-size:cover");

        primaryStage.setTitle("Messmate membership request");
        Scene sc = new Scene(vbOuter,1270,620);
        primaryStage.setScene(sc);
        primaryStage.show();
    }

}
