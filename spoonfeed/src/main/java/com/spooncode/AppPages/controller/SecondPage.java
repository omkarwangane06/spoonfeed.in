package com.spooncode.AppPages.controller;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import com.google.cloud.firestore.DocumentSnapshot;
import com.spooncode.AppPages.firebaseConfigHome.DataServiceHome;
import com.spooncode.SignupLogin.controller.LoginController;
import com.spooncode.SignupLogin.initialize.InitializeApp;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class SecondPage {
    LoginController loginController; // this will change as per sakshi's code (to maintain common stage)
    InitializeApp initializeApp;// for primary stage
    public Stage primaryStage;
    public static String messUserName;

    public SecondPage(Stage primaryStage, String messUserName)
            throws InterruptedException, ExecutionException, IOException {
        this.primaryStage = primaryStage;// now, i'm using scene of initilizeApp, but i've to make use of sakshi's
        this.messUserName = messUserName;
        sceneSecond();
    }

    public void sceneSecond() throws InterruptedException, ExecutionException, IOException {
        // stage design

        primaryStage.setTitle("MessMate");
        primaryStage.getIcons().add(new Image("./images/cooking.png"));

        // elements on scene

        DropShadow shadow = new DropShadow();

        Label welcome = new Label("Welcome to");
        welcome.setFont(Font.font("Arial", 25));
        welcome.setStyle("-fx-font-weight:bold;-fx-text-fill:#FF8C00");

        DocumentSnapshot docSnap = DataServiceHome.messData(messUserName);

        Label messName = new Label((String) docSnap.get("messName"));
        messName.setStyle("-fx-font-weight:bold;-fx-font-size:30px;-fx-font-fill:black");

        DocumentSnapshot documentSnapshot = DataServiceHome.messData(messUserName);

        Image img = new Image(documentSnapshot.getString("logo"));
        ImageView imgView = new ImageView(img);
        imgView.setFitHeight(150);
        imgView.setFitWidth(200);
        imgView.setPreserveRatio(true);

        VBox vb0 = new VBox(2, imgView, welcome, messName);
        vb0.setAlignment(Pos.CENTER);

        Label lb1 = new Label("Today's Menu for you-");
        lb1.setStyle("-fx-font-weight:bold;-fx-font-size:20px;-fx-text-fill:#FF8C00");

        DocumentSnapshot menuSnapshot = DataServiceHome.messMenu(messUserName);

        Label item1 = new Label((String) menuSnapshot.get("item1"));
        item1.setStyle("-fx-font-size:15px;-fx-font-weight:bold;-fx-text-fill:black");
        HBox menu1 = new HBox(10, item1);
        menu1.setAlignment(Pos.CENTER);

        Label item2 = new Label((String) menuSnapshot.get("item2"));
        item2.setStyle("-fx-font-size:15px;-fx-font-weight:bold;-fx-text-fill:black");
        HBox menu2 = new HBox(10, item2);
        menu2.setAlignment(Pos.CENTER);

        Label item3 = new Label((String) menuSnapshot.get("item3"));
        item3.setStyle("-fx-font-size:15px;-fx-font-weight:bold;-fx-text-fill:black");
        HBox menu3 = new HBox(10, item3);
        menu3.setAlignment(Pos.CENTER);

        Label item4 = new Label((String) menuSnapshot.get("item4"));
        item4.setStyle("-fx-font-size:15px;-fx-font-weight:bold;-fx-text-fill:black");
        HBox menu4 = new HBox(10, item4);
        menu4.setAlignment(Pos.CENTER);

        Label item5 = new Label((String) menuSnapshot.get("item5"));
        item5.setStyle("-fx-font-size:15px;-fx-font-weight:bold;-fx-text-fill:black");
        HBox menu5 = new HBox(10, item5);
        menu5.setAlignment(Pos.CENTER);

        VBox vbOuter = new VBox(10, lb1, menu1, menu2, menu3, menu4, menu5);

        Image menuIcon = new Image("./images/FoodPlate.png");
        ImageView imageView = new ImageView(menuIcon);
        imageView.setFitHeight(89);
        imageView.setFitWidth(90);
        imageView.setPreserveRatio(true);
        imageView.setOpacity(0);

        Label price0 = new Label("Price");

        Label price = new Label();
        price.setText((String) menuSnapshot.get("price"));
        price.setStyle("-fx-font-size:25px;-fx-font-weight:bold;");

        VBox priceBox = new VBox(price0, price);
        priceBox.setAlignment(Pos.CENTER);

        HBox MenuBox = new HBox(50, imageView, vbOuter);
        MenuBox.setAlignment(Pos.CENTER);
        MenuBox.setLayoutX(400);
        vbOuter.setAlignment(Pos.CENTER);


        DocumentSnapshot snapshot2=DataServiceHome.messData(messUserName);

        Label address=new Label("Address: "+snapshot2.getString("messAddress"));
        address.setStyle("-fx-font-size:15;-fx-font-weight:bold");


        Label label = new Label("We are here to take care of your hunger!");
        label.setStyle("-fx-font-size:17px;-fx-font-weight:bold");

        Label label2 = new Label("be our regular member:");
        label2.setStyle("-fx-font-size:15px;-fx-font-weight:bold;-fx-text-fill:#FF8C00;-fx-font-size:15");// #FF8C00

        Label label3 = new Label("MessMate");
        label3.setStyle("-fx-font-size:12px;-fx-text-fill:Grey");

        // Order and membership
        Button back = new Button("Mess List");
        back.setMaxSize(120, 30);
        back.setMinSize(110, 30);
        back.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FirstPage.initFirstPage();
            }
        });
        back.setEffect(shadow);
        back.setWrapText(true);
        Button membership = new Button("Take Membership");
        membership.setMaxSize(120, 30);
        membership.setMinSize(110, 30);
        membership.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                try {
                    RequestSignup requestSignup = new RequestSignup(primaryStage, messUserName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        membership.setEffect(shadow);
        membership.setWrapText(true);

        Button login = new Button("Login");
        login.setMinSize(100, 30);
        login.setStyle("-fx-background-fill:Black");
        login.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                LoginController loginController = new LoginController(primaryStage, messUserName);
                primaryStage.setScene(loginController.getLoginScene());

            }

        });
        login.setEffect(shadow);
        login.setWrapText(true);

        VBox vb1 = new VBox(back);
        VBox vb2 = new VBox(membership);
        VBox vb3 = new VBox(login);

        HBox buttonBox = new HBox(100, vb1, vb2, vb3);
        buttonBox.setPadding(new Insets(0, 100, 0, 100));
        buttonBox.setAlignment(Pos.CENTER);
        VBox vbBottom = new VBox(7, label, label2, buttonBox);
        vbBottom.setAlignment(Pos.CENTER);

        // This image is only to give some space between menuBox and vbBottom
        ImageView imgBlank = new ImageView(menuIcon);
        imgBlank.setFitHeight(10);
        imgBlank.setFitWidth(10);
        imgBlank.setOpacity(0);

        VBox vbP2 = new VBox(19, vb0, MenuBox, imgBlank,address, vbBottom, label3);
        vbP2.setStyle("-fx-background-image:url('./images/abc2.jpg');-fx-background-size:cover");
        vbP2.setAlignment(Pos.CENTER);
        vbP2.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));

        VBox vbOuter2 = new VBox(10, vbP2);
        vbOuter2.setStyle("-fx-background-image:url('./images/abc.jpg');-fx-background-size:cover");// xyz2.png
        vbOuter2.setPadding(new Insets(30, 300, 30, 300));
        vbOuter2.setAlignment(Pos.CENTER);

        Image FoodPlate = new Image("./images/FoodPlate.png");
        ImageView FoodPlateView = new ImageView(FoodPlate);
        FoodPlateView.setPreserveRatio(true);
        FoodPlateView.setFitHeight(160);
        FoodPlateView.setFitWidth(160);
        FoodPlateView.setLayoutX(800);
        FoodPlateView.setLayoutY(400);

        Image menu = new Image("./images/menuTag.png");
        ImageView menuTag = new ImageView(menu);
        menuTag.setPreserveRatio(true);
        menuTag.setFitHeight(40);
        menuTag.setFitWidth(100);

        Image frame = new Image("./images/flower.png");
        ImageView frameView = new ImageView(frame);
        frameView.setPreserveRatio(false);
        frameView.setFitHeight(280);
        frameView.setFitWidth(700);
        frameView.setOpacity(0.2);

        StackPane stackPane = new StackPane(vbOuter2, FoodPlateView, priceBox, menuTag, frameView);
        stackPane.setAlignment(FoodPlateView, Pos.CENTER_RIGHT);
        stackPane.setMargin(FoodPlateView, new Insets(145, 620, 0, 0));
        stackPane.setAlignment(priceBox, Pos.CENTER_LEFT);
        stackPane.setMargin(priceBox, new Insets(150, 0, 0, 340));
        stackPane.setAlignment(menuTag, Pos.CENTER_LEFT);
        stackPane.setMargin(menuTag, new Insets(10, 0, 0, 450));
        stackPane.setMargin(frameView, new Insets(75, 0, 0, 0));

        Scene scP2 = new Scene(stackPane, 1270, 620);
        primaryStage.setScene(scP2);
        primaryStage.show();
    }
}
