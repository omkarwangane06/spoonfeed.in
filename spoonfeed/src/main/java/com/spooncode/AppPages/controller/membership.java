package com.spooncode.AppPages.controller;

import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

class InvalidDataException extends Exception {
    InvalidDataException(String message) {
        super(message);
    }
}

public class membership {
    static Stage primaryStage;
    static HBox hb3;

    public membership(Stage primaryStage) {
        membership.primaryStage = primaryStage;
        memberReq();
    }

    public static void memberReq() {

        Label lab = new Label("!! Add New Member !!");
        lab.setFont(new javafx.scene.text.Font(25));
        HBox hb = new HBox(lab);
        hb.setAlignment(Pos.CENTER);

        Label lab1 = new Label("Username :");
        TextField tf1 = new TextField();
        HBox hb1 = new HBox(25, lab1, tf1);
        hb1.setAlignment(Pos.CENTER);

        Label lab2 = new Label("Join Date :");
        TilePane r = new TilePane();
        DatePicker d = new DatePicker();
        r.getChildren().add(d);
        HBox hb2 = new HBox(25, lab2, d);
        hb2.setAlignment(Pos.CENTER);

        Label lab3 = new Label("Phone Number :");
        TextField tf2 = new TextField();

        hb3 = new HBox(25, lab3, tf2);
        hb3.setAlignment(Pos.CENTER);

        Button submitButton = new Button("Submit");
        submitButton.setAlignment(Pos.CENTER);

        submitButton.setOnAction(new javafx.event.EventHandler<ActionEvent>() {

            public void handle(ActionEvent event) {

                LocalDate selectedDate = d.getValue();
                String username = tf1.getText();
                String phoneNumber = tf2.getText();

                if (selectedDate != null && tf1 != null && tf2 != null) {
                    System.out.println("Joining Date is " + selectedDate);
                    System.out.println("The Username is " + username);
                    System.out.println("The Phone Number is " + phoneNumber);

                } else {
                    try {
                        throw new InvalidDataException("Incomplete data");
                    } catch (InvalidDataException e) {
                        System.out.println(e);
                        String hats=e.toString();
                        showAlert("Invalid credentials",hats);
                    }
                    // showAlert("Invalid Data", "Fill the above Data..!!");
                }
            }
        });
        VBox vb = new VBox(20, hb, hb1, hb2, hb3, submitButton);
        vb.setAlignment(Pos.CENTER);

        Scene sc = new Scene(vb, 700, 800);
        primaryStage.setScene(sc);
        primaryStage.show();
    }

    private static void showAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setResizable(false);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
}