package com.spooncode.AppPages.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class FirstPage {
    private static Stage primaryStage;
    // private static Firestore db;
    public static String messUserName;

    public FirstPage(Stage primaryStage) {
        FirstPage.primaryStage = primaryStage;
        initFirstPage();
    }

    static void initFirstPage() {

        primaryStage.setTitle("MessMate Home");

        Image img = new Image("./images/cooking.png");
        ImageView imageView = new ImageView(img);
        imageView.setFitHeight(120);
        imageView.setFitWidth(120);
        imageView.setPreserveRatio(true);

        Label welcome = new Label();
        welcome.setText("Welcome to MessMate");
        welcome.setFont(Font.font(15));
        welcome.setStyle("-fx-font-weight:bold;-fx-text-fill:#001f3f");

        Label messMate = new Label("MessMate");
        messMate.setFont(Font.font(15));
        messMate.setStyle("-fx-font-weight:bold;-fx-text-fill:#001f3f");// NavyBlue

        Label slogan = new Label("Find Your Ideal Dining Destination!");
        slogan.setFont(Font.font(24));
        slogan.setStyle("-fx-font-weight:bold;-fx-text-fill:#FF8C00");// orange

        VBox vb1 = new VBox(4, welcome, slogan);
        vb1.setAlignment(Pos.CENTER);

        DropShadow shadow = new DropShadow();

        Button mess1 = new Button("Aai Mess");
        mess1.setFont(Font.font(20));
        mess1.setMinSize(400, 30);
        mess1.setStyle("-fx-font-weight:bold;-fx-background-color:#FF8C00;-fx-text-fill:White");
        mess1.setMinHeight(45);
        Image mess1Img = new Image("./images/AaiMess.png");
        ImageView imgView1 = new ImageView(mess1Img);
        imgView1.setFitHeight(60);
        imgView1.setFitWidth(60);
        imgView1.setPreserveRatio(true);
        HBox mess1Hbox = new HBox(5, imgView1, mess1);
        mess1Hbox.setAlignment(Pos.CENTER);
        mess1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                messUserName = "AaiMess";

                try {
                    SecondPage secondPage = new SecondPage(primaryStage, messUserName);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

        });
        mess1.setEffect(shadow);
        mess1.setWrapText(true);

        Button mess2 = new Button("Jagdamba Bhojnalay ");
        mess2.setFont(Font.font(20));
        mess2.setMinSize(400, 30);
        mess2.setStyle("-fx-font-weight:bold;-fx-background-color:#FF8C00;-fx-text-fill:White");
        mess2.setMinHeight(45);
        Image mess2Img = new Image("./images/JagdambaMess.jpg");
        ImageView imgView2 = new ImageView(mess2Img);
        imgView2.setFitHeight(60);
        imgView2.setFitWidth(60);
        imgView2.setPreserveRatio(true);
        HBox mess2Hbox = new HBox(5, imgView2, mess2);
        mess2Hbox.setAlignment(Pos.CENTER);
        mess2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                messUserName = "JagdambaKhanaval";

                try {
                    SecondPage secondPage = new SecondPage(primaryStage, messUserName);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

        });
        mess2.setEffect(shadow);
        mess2.setWrapText(true);

        Button mess3 = new Button("Jai Bhavani ");
        mess3.setFont(Font.font(20));
        mess3.setMinSize(400, 30);
        mess3.setStyle("-fx-font-weight:bold;-fx-background-color:#FF8C00;-fx-text-fill:white");
        mess3.setMinHeight(45);
        Image mess3Img = new Image("./images/cooking.png");
        ImageView imgView3 = new ImageView(mess3Img);
        imgView3.setFitHeight(60);
        imgView3.setFitWidth(60);
        imgView3.setPreserveRatio(true);
        HBox mess3Hbox = new HBox(5, imgView3, mess3);
        mess3Hbox.setAlignment(Pos.CENTER);
        mess3.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                messUserName = "JaiBhavani";

                try {
                    SecondPage secondPage = new SecondPage(primaryStage, messUserName);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

        });

        mess3.setEffect(shadow);
        mess3.setWrapText(true);

        Button mess4 = new Button("Shree Khanaval");
        mess4.setFont(Font.font(20));
        mess4.setMinSize(400, 30);
        mess4.setStyle("-fx-font-weight:bold;-fx-background-color:#FF8C00;-fx-text-fill:white");// #FF9913
        mess4.setMinHeight(45);
        Image mess4Img = new Image("./images/ShreeMess.png");
        ImageView imgView4 = new ImageView(mess4Img);
        imgView4.setFitHeight(60);
        imgView4.setFitWidth(60);
        imgView4.setPreserveRatio(true);
        HBox mess4Hbox = new HBox(5, imgView4, mess4);
        mess4Hbox.setAlignment(Pos.CENTER);
        mess4.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                messUserName = "ShreeMess";
                try {
                    SecondPage secondPage = new SecondPage(primaryStage, messUserName);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

        });
        mess4.setEffect(shadow);
        mess4.setWrapText(true);
        Button mess5 = new Button("Abhi Kitchen");
        mess5.setFont(Font.font(20));
        mess5.setMinSize(400, 30);
        mess5.setStyle("-fx-font-weight:bold;-fx-background-color:#FF8C00;-fx-text-fill:white");
        mess5.setMinHeight(45);
        Image mess5Img = new Image("./images/cooking.png");
        ImageView imgView5 = new ImageView(mess5Img);
        imgView5.setFitHeight(60);
        imgView5.setFitWidth(60);
        imgView5.setPreserveRatio(true);
        HBox mess5Hbox = new HBox(5, imgView5, mess5);
        mess5Hbox.setAlignment(Pos.CENTER);
        mess5.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                messUserName = "SaiMess";

                try {
                    SecondPage secondPage = new SecondPage(primaryStage, messUserName);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

        });

        mess5.setEffect(shadow);
        mess5.setWrapText(true);

        VBox messBox = new VBox(15, mess1Hbox, mess2Hbox, mess3Hbox, mess4Hbox, mess5Hbox);
        messBox.setAlignment(Pos.CENTER);

        // FOOTER

        Label adminContact = new Label("if you want to register your mess,call on: 8456746382");
        adminContact.setStyle("-fx-text-fill:Blue;");
        Label thumbnell = new Label("MessMate");
        thumbnell.setStyle("-fx-text-fill:Grey");
        VBox bottom = new VBox(adminContact, thumbnell);

        // ScrollBar
        ScrollBar scrollBar = new ScrollBar();
        scrollBar.setOrientation(Orientation.VERTICAL);
        scrollBar.setMin(0);
        scrollBar.setMax(800);
        scrollBar.setPrefHeight(620);
        scrollBar.setLayoutX(1250);
        scrollBar.setStyle("-fx-pref-width:20");

        /*
         * Image imgSide=new Image("./images/firstPageBack.jpg");
         * ImageView imgViewSide=new ImageView(imgSide);
         * imgViewSide.setPreserveRatio(true);
         * imgViewSide.setFitHeight(400);
         * imgViewSide.setFitWidth(300);
         */

         Button about=new Button("About Team");
         about.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
              aboutUs();
            }
            
         });

        VBox vbOuter = new VBox(20, imageView, vb1, messBox, adminContact,about, thumbnell);
        vbOuter.setAlignment(Pos.CENTER);

        vbOuter.setStyle(
                "-fx-background-image:url('./images/abc.jpg');-fx-background-opacity:0.2;-fx-background-size:cover");
        vbOuter.setMinWidth(1250);

        scrollBar.valueProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                vbOuter.setLayoutY(-newValue.doubleValue() + 4);
            }

        });

        Pane pane = new Pane(vbOuter, scrollBar);

        Scene scP1 = new Scene(pane, 1270, 620);
        primaryStage.setScene(scP1);
        primaryStage.show();

    }

    static void aboutUs(){
        VBox aboutUsLayout = new VBox(10);
        aboutUsLayout.getChildren().addAll(
            new Label("About Us"),
            new Label("This application is developed by The WorkHolics."),
            new Label("Our mission is to create high-quality softwares.")
        );


        Image imageSachinSir = new Image("./images/SachinSir.jpeg");
        ImageView imageViewSachinSir = new ImageView(imageSachinSir);
        imageViewSachinSir.setFitHeight(200);
        imageViewSachinSir.setFitWidth(200);

        Image imageShashiSir = new Image("./images/ShashiSir.jpeg");
        ImageView imageViewShashiSir = new ImageView(imageShashiSir);
        imageViewShashiSir.setFitHeight(200);
        imageViewShashiSir.setFitWidth(300);

        Label shashiSir=new Label("Shri.Shashi Sir");
        shashiSir.setStyle("-fx-font-size:20;-fx-font-weight:bold");
        Label sachinSir=new Label("Shri.Sachin Sir (Mentor)");
        sachinSir.setStyle("-fx-font-size:20;-fx-font-weight:bold");
         
        VBox vb1=new VBox(5,imageViewShashiSir,shashiSir);
        

        VBox vb2=new VBox(5,imageViewSachinSir,sachinSir);
        vb1.setAlignment(Pos.CENTER);
        vb2.setAlignment(Pos.CENTER);
        HBox MentorHbox=new HBox(50,vb1,vb2);

        MentorHbox.setAlignment(Pos.CENTER);

        Label thanks=new Label("Thank you for your guidance!");
        thanks.setStyle("-fx-font-size:22;-fx-font-weight:bold");
        VBox mentorFinalBox=new VBox(10,thanks,MentorHbox);
//mentorFinalBox
mentorFinalBox.setAlignment(Pos.CENTER);


        Label messMate=new Label("MessMate ");
        messMate.setStyle("-fx-font-size:25;-fx-font-weight:bold;-fx-text-fill:#FF8C00");
        Label by=new Label("By-The Workaholics ");
        by.setStyle("-fx-font-size:18;-fx-font-weight:bold;-fx-text-fill:black");

        VBox vb4=new VBox(5,messMate,by);
        vb4.setAlignment(Pos.CENTER);


        Label ourTeam=new Label("Our Team  ");
        ourTeam.setStyle("-fx-font-size:18;-fx-font-weight:bold;-fx-text-fill:#FF8C00");
        
        Label sakshi=new Label("Parkale Sakshi Sanjay");
        sakshi.setStyle("-fx-font-size:18;-fx-font-weight:bold;-fx-text-fill:black");
        Label jivan=new Label("Magare Eknath Jivan");
        jivan.setStyle("-fx-font-size:18;-fx-font-weight:bold;-fx-text-fill:black");
        Label shreyas=new Label("Narlawar Shreyas Shrihari");
        shreyas.setStyle("-fx-font-size:18;-fx-font-weight:bold;-fx-text-fill:black");
        Label omkar=new Label("Wangane Omkar Ramesh");
        omkar.setStyle("-fx-font-size:18;-fx-font-weight:bold;-fx-text-fill:black");


        VBox team=new VBox(5,ourTeam,sakshi,jivan,shreyas,omkar);
        team.setAlignment(Pos.CENTER);

        Label thanksAll=new Label("Thank you to all who directly,indirectly help up to build our project and enhance our knowledge.");
        thanksAll.setStyle("-fx-font-size-20;-fx-font-weight:bold");

        VBox finalBox=new VBox(20,mentorFinalBox,vb4,team,thanksAll);
        finalBox.setAlignment(Pos.CENTER);



        Scene aboutUsScene = new Scene(finalBox, 1270, 620);

        primaryStage.setScene(aboutUsScene);
        primaryStage.setTitle("About Us");
        primaryStage.show();
    }

    }


