package com.spooncode.AppPages.controller;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFunction;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.spooncode.SignupLogin.firebaseConfig.DataService;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class AddNewMess {
    public Stage primaryStage;
    private Firestore db;
    TextField ownerUserNameField;
    TextField messNameField;
    TextField ownerNameField;
    TextField messTimeField;
    TextField messAddressField;
    TextField isNonVegField;
    TextField whenHolidays;
    TextField messFeesField;
    TextField contactField;
    TextField extraRulesField;
    PasswordField passwordField;
    TextField logoField;
    public static int flag = 0;

    public AddNewMess(Stage primaryStage) {
        this.primaryStage = primaryStage;
        initAddNewMess();
    }

    public void initAddNewMess() {
        db = DataService.db;

        primaryStage.setTitle("MessMate (Admin, Add new mess)");

        Image img = new Image("./images/plus.png");
        ImageView imgView = new ImageView(img);
        imgView.setFitHeight(40);
        imgView.setFitWidth(40);
        Label heading = new Label("Admin,Add new Mess...");
        heading.setFont(Font.font(30));
        heading.setStyle("-fx-font-weight:bold");
        HBox headBox = new HBox(10, imgView, heading);
        headBox.setAlignment(Pos.CENTER);

        Label messName = new Label("Enter Mess Name:");
        messName.setStyle("-fx-font-weight:bold");
        messNameField = new TextField();
        messNameField.setMaxSize(500, 20);
        VBox vb1 = new VBox(5, messName, messNameField);
        vb1.setAlignment(Pos.CENTER);

        Label ownerName = new Label("Enter owner name:");
        ownerName.setStyle("-fx-font-weight:bold");
        ownerNameField = new TextField();
        ownerNameField.setMaxSize(500, 20);
        VBox vb2 = new VBox(5, ownerName, ownerNameField);
        vb2.setAlignment(Pos.CENTER);

        Label ownerUserName = new Label("Enter Owner username:");
        ownerUserName.setStyle("-fx-font-weight:bold");
        ownerUserNameField = new TextField();
        ownerUserNameField.setMaxSize(500, 20);
        VBox vb3 = new VBox(5, ownerUserName, ownerUserNameField);
        vb3.setAlignment(Pos.CENTER);

        Label password = new Label("Give password to messOwner");
        password.setStyle("-fx-font-weight:bold");
        passwordField = new PasswordField();
        passwordField.setMaxSize(500, 20);
        VBox vb4 = new VBox(5, password, passwordField);
        vb4.setAlignment(Pos.CENTER);

        Label messTime = new Label("Enter mess Timing:");
        messTime.setStyle("-fx-font-weight:bold");
        messTimeField = new TextField();
        messTimeField.setMaxSize(500, 20);
        VBox vb5 = new VBox(5, messTime, messTimeField);
        vb5.setAlignment(Pos.CENTER);

        Label messAddress = new Label("Enter mess address");
        messAddress.setStyle("-fx-font-weight:bold");
        messAddressField = new TextField();
        messAddressField.setMaxSize(500, 20);
        messAddressField.setPromptText("Mess Address");
        VBox vb6 = new VBox(5, messAddress, messAddressField);
        vb6.setAlignment(Pos.CENTER);

        Label isNonVeg = new Label("Non veg or not");
        isNonVeg.setStyle("-fx-font-weight:bold");
        isNonVegField = new TextField();
        isNonVegField.setMaxSize(500, 20);
        VBox vb7 = new VBox(5, isNonVeg, isNonVegField);
        vb7.setAlignment(Pos.CENTER);

        Label whenHoliday = new Label("When hoidays?");
        whenHoliday.setStyle("-fx-font-weight:bold");
        whenHolidays = new TextField();
        whenHolidays.setMaxSize(500, 20);
        VBox vb8 = new VBox(5, whenHoliday, whenHolidays);
        vb8.setAlignment(Pos.CENTER);

        Label messFees = new Label("Mess Fees(30 days)");
        messFees.setStyle("-fx-font-weight:bold");
        messFeesField = new TextField();
        messFeesField.setMaxSize(500, 20);
        VBox vb9 = new VBox(5, messFees, messFeesField);
        vb9.setAlignment(Pos.CENTER);

        Label contact = new Label("Owner contact number: ");
        contact.setStyle("-fx-font-weight:bold");
        contactField = new TextField();
        contactField.setMaxSize(500, 20);
        VBox vb10 = new VBox(5, contact, contactField);
        vb10.setAlignment(Pos.CENTER);

        Label extraRules = new Label("Additional rules of mess:");
        extraRules.setStyle("-fx-font-weight:bold");
        extraRulesField = new TextField();
        extraRulesField.setMaxSize(500, 20);
        VBox vb11 = new VBox(5, extraRules, extraRulesField);
        vb11.setAlignment(Pos.CENTER);

        Label logoLabel = new Label("Enter address of logo");
        logoLabel.setStyle("-fx-font-weight:bold");
        // logoField = new TextField();
        // logoField.setMaxSize(500, 20);
        // VBox vb12 = new VBox(5, logoLabel, logoField);
        // vb12.setAlignment(Pos.CENTER);
        // Button selectImageButton = new Button("SelectImage");
        // FileChooser fileChooser = new FileChooser();
        // fileChooser.setTitle("Select Image File");
        // fileChooser.getExtensionFilters().addAll(
        //         new FileChooser.ExtensionFilter("Image files", "*.png", "*.jpg", "*.jpeg"));

        // selectImageButton.setOnAction(event -> {
        //     File selectedFile = fileChooser.showOpenDialog(null);
        //     if (selectedFile != null) {
        //     }
        // });

        logoField=new TextField();
        logoField.setMaxSize(500,20);
        VBox vb12 = new VBox(5,logoLabel, logoField);
        vb12.setAlignment(Pos.CENTER);
        Button add = new Button("Add mess");
        add.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                try {
                    messData();
                    AdminPage adminPage = new AdminPage(primaryStage);
                } catch (ExecutionException | InterruptedException e) {

                    e.printStackTrace();
                }
            }

        });

        Button cancel = new Button("Cancel");

        cancel.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                AdminPage adminPage = new AdminPage(primaryStage);
            }

        });

        HBox hbox = new HBox(100, cancel, add);
        hbox.setAlignment(Pos.CENTER);
        Pane root = new Pane();

        VBox vb = new VBox(6, headBox, vb1, vb2, vb3, vb4, vb5, vb9, vb7, vb8, vb11, vb12, vb6, vb10, hbox);
        vb.setAlignment(Pos.CENTER);
        vb.setMinWidth(1250);
        vb.setLayoutY(50);

        ScrollBar scrollBar = new ScrollBar();
        scrollBar.setOrientation(Orientation.VERTICAL);
        scrollBar.setLayoutX(1250);
        scrollBar.setMin(0);
        scrollBar.setMax(800);
        scrollBar.setPrefHeight(640);
        scrollBar.setStyle("-fx-pref-width:20;");

        scrollBar.valueProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                vb.setLayoutY(-newValue.doubleValue() + 10);
            }

        });
        // VBox vbComBox=new VBox(vb,scrollBar);
        root.getChildren().addAll(vb, scrollBar);

        /*
         * vb.setLayoutY(-10);
         * vb.setMaxSize(600, 600);
         * vb.setPadding(new Insets(10));
         */

        Scene sc = new Scene(root, 1270, 620);

        primaryStage.setScene(sc);
        primaryStage.show();
    }

    public void messData() throws ExecutionException, InterruptedException {

        Map<String, Object> messDetails = new HashMap<>();
        System.out.println("Map messdata object is " + messDetails);
        messDetails.put("ownerUserName", ownerUserNameField.getText());
        messDetails.put("messName", messNameField.getText());
        messDetails.put("ownerName", ownerNameField.getText());
        messDetails.put("password", passwordField.getText());
        messDetails.put("messTime", messTimeField.getText());
        messDetails.put("messAddress", messAddressField.getText());
        messDetails.put("messFees", messFeesField.getText());
        messDetails.put("isNonVeg", isNonVegField.getText());
        messDetails.put("whenHoliday", whenHolidays.getText());
        messDetails.put("contact", contactField.getText());
        messDetails.put("extraRules", extraRulesField.getText());
        messDetails.put("logo", logoField.getText());
        messDetails.put("role","messOwner");

        String userName = ownerUserNameField.getText();

        ApiFuture<WriteResult> writeResult = db.collection("credentials").document("user").collection("messOwner")
                .document(userName).set(messDetails);
        // dataService.addData("credentials", "user", "messOwner", userName,
        // messDetails);
    }

}
